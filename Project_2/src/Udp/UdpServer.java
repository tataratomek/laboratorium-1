package Udp;
 
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

import frame.Packet;
import frame.Tools;

public class UdpServer {
    public static void main(String[] args) {
    	DatagramSocket aSocket = null;
      try {
        // args contain message content and server hostname
        aSocket = new DatagramSocket(9876);
        byte[] buffer = new byte[1024];
        while(true) {
          DatagramPacket request = new DatagramPacket(buffer, buffer.length);
          System.out.println("Waiting for request...");
          File read_file = new File("name_description_12.bin");
          try {
          System.out.println(Files.readAllBytes(read_file.toPath()));
          } catch (FileNotFoundException e)
          {
        	  System.out.println("file exception!");
          }
          aSocket.receive(request);

          try {
				Packet read = (Packet)Tools.deserialize(request.getData());
				Files.write(new File(read.getFileName()).toPath(), request.getData());
		        System.out.println("server: ");
				System.out.println(read.toString());
			} catch (ClassNotFoundException e) {
				System.out.println("server:  Deserialize exception");
			}	
          
          
//          DatagramPacket reply = new DatagramPacket(request.getData(), request.getLength(), 
//          		request.getAddress(), request.getPort());
//          aSocket.send(reply); 
        }
      } catch (SocketException ex) {
        Logger.getLogger(UdpServer.class.getName()).log(Level.SEVERE, null, ex);
      } catch (IOException ex) {
        Logger.getLogger(UdpServer.class.getName()).log(Level.SEVERE, null, ex);
      } finally {
				aSocket.close();
			}
      
    }
}

