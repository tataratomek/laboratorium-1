package Udp; 

import java.io.IOException;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import frame.Data_array;
import frame.Packet;
import frame.TimeHistory;
import frame.Tools;


public class UDPClient {
	public static void main(String[] args) {
		DatagramSocket aSocket = null;
		Scanner scanner = null;
		
		//user implementation 1 start//
		int[] src = new int[10];
		src[0] = 1;
		src[1] = 10;
		Data_array array = new Data_array(src);
		TimeHistory<Data_array> packet = new TimeHistory<Data_array>("name", "description", 12, 0, "mm", 0.12, array, 1);
		byte[] data = Tools.serialize((TimeHistory<Data_array>)packet);

		//user implementation 1 end//
		try {
			// args contain server hostname
			if (args.length < 1) {
				System.out.println("Usage: UDPClient <server host name>");
				System.exit(-1);
			}

			InetAddress aHost = InetAddress.getLocalHost();//(args[0]);
			int serverPort = 9876;
			//user implementation 2 start//
			DatagramPacket datagram = new DatagramPacket(data, data.length, aHost, serverPort);
			//user implementation 2 end//
			aSocket = new DatagramSocket();
			scanner = new Scanner(System.in);
			
			aSocket.send(datagram);
			byte[] reply_data = new byte[data.length];
			DatagramPacket reply = new DatagramPacket(reply_data, reply_data.length);
			aSocket.receive(reply);
			
			try {
				Packet read = (Packet)Tools.deserialize(reply.getData());
				System.out.println("client: ");
				System.out.println(read.toString());
			} catch (ClassNotFoundException e) {
				System.out.println("client: Deserialize exception");
			}				

	
		} catch (SocketException ex) {
			Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnknownHostException ex) {
			Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			aSocket.close();
			scanner.close();
		}
	}
}