package frame;

public class Alarm extends Packet{
	int channelNr;
	int treshold;
	int direction;
	public Alarm(String name, String description, long date, int channelNr, int treshold, int direction) {
		super(name, description, date);
		this.channelNr = channelNr;
		this.treshold = treshold;
		this.direction = direction;
	}
	@Override
	public String toString() {
		return "Alarm [channelNr=" + channelNr + ", treshold=" + treshold + ", direction=" + direction + "]";
	}
	
}
