package frame;

public class TimeHistory<T> extends Sequence<T> {
	double sensitivity;



	public TimeHistory(String name, String description, long date, int channelNr, String unit, double resolution,
			T buffer, double sensitivity) {
		super(name, description, date, channelNr, unit, resolution, buffer);
		this.sensitivity = sensitivity;
	}



	@Override
	public String toString() {
		return "TimeHistory [sensitivity=" + sensitivity + ", channelNr=" + channelNr + ", unit=" + unit
				+ ", resolution=" + resolution + ", buffer=" + buffer + ", name=" + name + ", description="
				+ description + ", date=" + date + "]";
	}
	
	
}
