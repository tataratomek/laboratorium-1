package frame;

public abstract class Sequence<T> extends Packet {
	protected int channelNr;
	protected String unit;
	protected double resolution;
	protected T buffer;
	
	public Sequence(String name, String description, long date, int channelNr, String unit, double resolution, T buffer) {
		super(name, description, date);
		this.channelNr = channelNr;
		this.unit = unit;
		this.resolution = resolution;
		this.buffer = buffer;
	}

	@Override
	public String toString() {
		return "Sequence [channelNr=" + channelNr + ", unit=" + unit + ", resolution=" + resolution + ", buffer="
				+ buffer + "]";
	}
	
	
}
