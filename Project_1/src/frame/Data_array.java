package frame;
import java.io.Serializable;
import java.util.Arrays;

public class Data_array implements Serializable{
	int[] data = new int[10];

	public Data_array(int[] data) {
		super();
		this.data = data;
	}

	@Override
	public String toString() {
		return "Data_array [data=" + Arrays.toString(data) + "]";
	}
}
