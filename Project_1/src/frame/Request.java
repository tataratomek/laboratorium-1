package frame;

public abstract class Request<T> extends Packet {
	int type;
	int value;
	public Request(String name, String description, long date, int type, int value) {
		super(name, description, date);
		this.type = type;
		this.value = value;
	}
	@Override
	public String toString() {
		String str = "";
		if(type == 1)
			str = "get match";
		else if(type == 2)
			str = "after date";
		else if(type == 2)
			str = "before date";
		return "Request [type=" + str + ", value=" + value + ", name=" + name + ", description=" + description
				+ ", date=" + date + "]";
	}
	
}
