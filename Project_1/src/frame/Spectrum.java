package frame;

public class Spectrum<T> extends Sequence<T> {
	int scaling;



	public Spectrum(String name, String description, long date, int channelNr, String unit, double resolution, T buffer,
			int scaling) {
		super(name, description, date, channelNr, unit, resolution, buffer);
		this.scaling = scaling;
	}



	@Override
	public String toString() {
		return "Spectrum [scaling=" + scaling + "]";
	}
	
	
}
